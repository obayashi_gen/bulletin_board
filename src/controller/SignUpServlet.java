package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<Branch> branches = new BranchService().getBranch();
		request.setAttribute("branches", branches);

		List<Position> positions = new PositionService().getPosition();
		request.setAttribute("positions", positions);

		request.getRequestDispatcher("/signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		User signUpUser = getSignUpUser(request);

		List<Branch> branches = new BranchService().getBranch();
		request.setAttribute("branches", branches);

		List<Position> positions = new PositionService().getPosition();
		request.setAttribute("positions", positions);

		if (isValid(request, messages) == true) {
			new UserService().register(signUpUser);

			response.sendRedirect("./userManagement");
		} else {
			request.setAttribute("signUpUser", signUpUser);
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private User getSignUpUser(HttpServletRequest request)
			throws IOException, ServletException {

		User signUpUser = new User();
		signUpUser.setLoginId(request.getParameter("loginId"));
		signUpUser.setPassword(request.getParameter("password"));
		signUpUser.setName(request.getParameter("name"));
		signUpUser.setBranchId(Integer.parseInt(request.getParameter("branch")));
		signUpUser.setPositionId(Integer.parseInt(request.getParameter("position")));

		return signUpUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String checkPassword = request.getParameter("checkPassword");
		String name = request.getParameter("name");

		User user = new UserService().getUser(loginId);

		if (user != null) {
			messages.add("ログインIDが重複しています");
		}

		if (StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインIDを入力してください");
		}

		if (!(loginId.matches("^[0-9a-zA-Z]{6,20}+$"))) {
			messages.add("ログインIDは半角英数字で6文字以上20文字以下にしてください");
		}

		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		}

		if (!(password.matches("^[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]+$"))){
			messages.add("パスワードは記号を含む全ての半角文字で6文字以上20文字以下にしてください");
		}

		if (!(password.equals(checkPassword))) {
			messages.add("入力されたパスワードが一致しません");
		}

		if (StringUtils.isEmpty(name) == true) {
			messages.add("名称を入力してください");
		}

		if (name.length() > 10) {
			messages.add("名称は10文字以下で入力してください");
		}

		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}