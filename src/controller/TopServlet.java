package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Branch;
import beans.Position;
import beans.User;
import beans.UserComment;
import beans.UserMessage;
import service.BranchService;
import service.CommentService;
import service.MessageService;
import service.PositionService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");

		boolean isShowMessageForm;
		if (loginUser != null) {
			isShowMessageForm = true;
		} else {
			isShowMessageForm = false;
		}

		Date day = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateNow = dateFormat.format(day);
		String firstDate = "2017-01-01 00:00:00";
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");

		if (request.getParameter("startDate") == "" || request.getParameter("startDate") == null) {
			startDate = firstDate;
		}

		if (request.getParameter("endDate") == "" || request.getParameter("endDate") == null) {
			endDate = dateNow;
		}

		String category = request.getParameter("category");
		int branchId =loginUser.getBranchId();
		List<Branch> branches = new BranchService().getBranch(branchId);
		Branch branch = branches.get(0);

		int positionId =loginUser.getPositionId();
		List<Position> positions = new PositionService().getPosition(positionId);
		Position position = positions.get(0);

		List<UserMessage> messages = new MessageService().getMessage(startDate, endDate, category);
		List<UserComment> comments = new CommentService().getComment();

		request.setAttribute("position", position);
		request.setAttribute("branch", branch);
		request.setAttribute("loginUser", loginUser);
		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);
		request.setAttribute("isShowMessageForm", isShowMessageForm);
		request.setAttribute("startDate", startDate);
		request.setAttribute("endDate", endDate);
		request.setAttribute("category", category);

		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}
}