package controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import beans.UserManagement;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.PositionService;
import service.UserManagementService;
import service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<Branch> branches = new BranchService().getBranch();
		List<Position> positions = new PositionService().getPosition();
		List<String> messages = new ArrayList<String>();
		List<UserManagement> users = new UserManagementService().getUser();
		request.setAttribute("users", users);

		String userId = request.getParameter("userId");

		if (isValidDoGet(request, messages, userId) == true) {
			int id = Integer.parseInt(userId);
			User editUser = new UserService().getUser(id);

			if (editUser == null) {
				messages.add("該当のユーザーは存在しません");
				request.getSession().setAttribute("errorMessages", messages);
				response.sendRedirect("./userManagement");
				return;
			}

			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);
			request.setAttribute("editUser", editUser);
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		} else {
			messages.add("該当のユーザーは存在しません");
			request.getSession().setAttribute("errorMessages", messages);
			response.sendRedirect("./userManagement");
		}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<Branch> branches = new BranchService().getBranch();
		List<Position> positions = new PositionService().getPosition();

		int id = Integer.parseInt(request.getParameter("id"));
		String editUserPassword = new UserService().getUser(id).getPassword();

		List<String> messages = new ArrayList<String>();
		User editUser = getEditUser(request);
		User loginUser = (User) request.getSession().getAttribute("loginUser");
		HttpSession session = request.getSession();

		if (isValid(request, messages, editUser) == true) {

			try {
				new UserService().update(editUser, editUserPassword);
				if (editUser.getId() == loginUser.getId()) {
					session.setAttribute("loginUser", editUser);
				}
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("settings");
			}

			response.sendRedirect("./userManagement");
		} else {
			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);
			request.setAttribute("editUserPassword", editUserPassword);
			request.setAttribute("editUser", editUser);
			request.setAttribute("loginUser", loginUser);
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();

		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLoginId(request.getParameter("loginId"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setName(request.getParameter("name"));
		editUser.setBranchId(Integer.parseInt(request.getParameter("branch")));
		editUser.setPositionId(Integer.parseInt(request.getParameter("position")));

		Date formatDate = null;
		try {
			formatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(request.getParameter("update_date"));
		} catch (ParseException e) {
			// フォーマット変換に失敗したらエラー
			e.printStackTrace();
		}

		editUser.setUpdateDate(formatDate);

		return editUser;
	}

	private boolean isValidDoGet(HttpServletRequest request, List<String> messages, String userId) {
		if (userId == null) {
			return false;
		}

		if ((userId.matches("[0-9]+$"))) {
			try {
				Integer.parseInt(userId);
			} catch (NumberFormatException e) {
				return false;
			}
		} else {
			return false;
		}
		return true;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages, User editUser) {
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String checkPassword = request.getParameter("checkPassword");
		int branchId = Integer.parseInt(request.getParameter("branch"));
		int positionId = Integer.parseInt(request.getParameter("position"));
		int headOfficeNum = 1;
		int regularEmployeeNum = 1;


		User user = new UserService().getUser(loginId);

		if (user != null && user.getId() != editUser.getId()) {
			messages.add("ログインIDが重複しています");
		}

		if (StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインIDを入力してください");
		}

		if (!(loginId.matches("^[0-9a-zA-Z]{6,20}+$"))) {
			messages.add("ログインIDは半角英数字で6文字以上20文字以下にしてください");
		}

		if ((StringUtils.isNotEmpty(password) == true) && !(password.matches("^[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]+$"))){
			messages.add("パスワードは記号を含む全ての半角文字で6文字以上20文字以下にしてください");
		}

		if (!(password.equals(checkPassword))) {
			messages.add("入力されたパスワードが一致しません");
		}

		if (StringUtils.isEmpty(name) == true) {
			messages.add("名称を入力してください");
		}

		if (name.length() > 10) {
			messages.add("名称は10文字以下で入力してください");
		}

		if (!(branchId == headOfficeNum) && !(positionId == regularEmployeeNum)) {
			messages.add("支社と部署・役職の組み合わせを正しく選択してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}