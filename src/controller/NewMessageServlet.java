package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

			request.getRequestDispatcher("/newMessage.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();
		Message message = getMessages(request);

		if (isValid(request, errorMessages) == true) {

			new MessageService().register(message);

			response.sendRedirect("./");
		} else {
			request.setAttribute("message", message);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("newMessage.jsp").forward(request, response);
		}
	}

	private Message getMessages(HttpServletRequest request)
			throws IOException, ServletException {

		Message message = new Message();
		message.setTitle(request.getParameter("title"));
		message.setText(request.getParameter("text"));
		message.setCategory(request.getParameter("category"));
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");
		message.setUserId(loginUser.getId());

		return message;
	}

	private boolean isValid(HttpServletRequest request, List<String> errorMessages) {
		String title = request.getParameter("title");
		String text = request.getParameter("text");
		String category = request.getParameter("category");

		if (StringUtils.isEmpty(title) == true) {
			errorMessages.add("件名を入力してください");
		}

		if (title.length() > 30) {
			errorMessages.add("件名は30文字以下で入力してください");
		}

		if (StringUtils.isEmpty(text) == true) {
			errorMessages.add("本文を入力してください");
		}

		if (text.length() > 1000) {
			errorMessages.add("本文は1000文字以下で入力してください");
		}

		if (StringUtils.isEmpty(category) == true) {
			errorMessages.add("カテゴリーを入力してください");
		}

		if (category.length() > 10) {
			errorMessages.add("カテゴリーは10文字以下で入力してください");
		}

		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (errorMessages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}