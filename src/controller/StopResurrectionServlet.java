package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.StopResurrectionService;

@WebServlet(urlPatterns = { "/stopResurrection" })
public class StopResurrectionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		int id = Integer.parseInt(request.getParameter("userId"));
		int resurrection = Integer.parseInt(request.getParameter("resurrection"));

		new StopResurrectionService().update(id, resurrection);

		response.sendRedirect("./userManagement");
	}
}
