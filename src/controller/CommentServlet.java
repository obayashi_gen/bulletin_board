package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		response.sendRedirect("./");
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		Comment comment = createCommentData(request);
		List<String> errorMessages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");
		String loginUserName = loginUser.getName();
		Date d = new Date();
		SimpleDateFormat d1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String dateNow = d1.format(d);

		boolean isSuccess = false;
		String responseJson = "";
		String errors = "";

		if(isValid(comment, errorMessages)) {
			// バリデーション成功
			int commentId = new CommentService().register(comment);

			// 成功情報を返す
			isSuccess = true;
			responseJson = String.format("{\"is_success\" : \"%s\", \"loginUserName\" : \"%s\", \"commentId\" : \"%s\", \"dateNow\" : \"%s\"}", isSuccess, loginUserName, commentId, dateNow);
		} else {
			// バリデーションエラー
			// ListをJsonの形にして返す
			errors = new ObjectMapper().writeValueAsString(errorMessages);
			responseJson = String.format("{\"is_success\" : \"%s\", \"errors\" : %s}", isSuccess, errors);
		}

		response.setContentType("application/json;charset=UTF-8");
		response.getWriter().write(responseJson);
	}

	/*
	 * Json文字列をJavaオブジェクトに変換するメソッド
	 */
	private Comment createCommentData(HttpServletRequest request) {
		String data = request.getParameter("comment");

		Comment comment = new Comment();
		try {
			comment = new ObjectMapper().readValue(data, Comment.class);
		} catch(Exception e) {
			e.printStackTrace();
		}

		return comment;
	}

	/*
	 * 入力データの検証を行うメソッド
	 */
	private boolean isValid(Comment comment, List<String> errorMessages) {
		if(StringUtils.isBlank(comment.getText())) {
			errorMessages.add("コメント本文が入力されていません");
		}

		if (comment.getText().length() > 500) {
			errorMessages.add("コメント本文は500文字以下で入力してください");
		}

		if(errorMessages.size() > 0) {
			return false;
		} else {
			return true;
		}
	}
}
