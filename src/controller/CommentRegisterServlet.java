package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import beans.Comment;

@WebServlet(urlPatterns = { "/comment/register" })
public class CommentRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		Comment comment = createCommentData(request);
		List<String> errorMessages = new ArrayList<String>();

		boolean isSuccess = false;
		String responseJson = "";
		String errors = "";

		if(isValid(comment, errorMessages)) {
			// バリデーション成功
			System.out.println("message_id = " + comment.getMessageId());
			System.out.println("user_id = " + comment.getUserId());
			System.out.println("text = " + comment.getText());
			// 成功情報を返す
			isSuccess = true;
			responseJson = String.format("{\"is_success\" : \"%s\"}", isSuccess);
		} else {
			// バリデーションエラー
			// ListをJsonの形にして返す
			errors = new ObjectMapper().writeValueAsString(errorMessages);
			responseJson = String.format("{\"is_success\" : \"%s\", \"errors\" : %s}", isSuccess, errors);
		}

		response.setContentType("application/json;charset=UTF-8");
		response.getWriter().write(responseJson);
	}

	/*
	 * Json文字列をJavaオブジェクトに変換するメソッド
	 */
	private Comment createCommentData(HttpServletRequest request) {
		String data = request.getParameter("comment");

		Comment comment = new Comment();
		try {
			comment = new ObjectMapper().readValue(data, Comment.class);
		} catch(Exception e) {
			e.printStackTrace();
		}

		return comment;
	}

	/*
	 * 入力データの検証を行うメソッド
	 */
	private boolean isValid(Comment comment, List<String> errorMessages) {
		if(StringUtils.isBlank(comment.getText())) {
			errorMessages.add("コメントを入力してください。");
			errorMessages.add("テストエラーメッセージ");
		} else if(comment.getText().length() > 10) {
			errorMessages.add("コメントは10文字以下で入力してください。");
		}

		if(errorMessages.size() > 0) {
			return false;
		} else {
			return true;
		}
	}

}
