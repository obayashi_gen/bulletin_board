package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserManagement;
import exception.SQLRuntimeException;

public class UserManagementDao {

	public List<UserManagement> getAllUser(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.name as user_name, ");
			sql.append("users.branch_id as branch_id, ");
			sql.append("branches.name as branch_name, ");
			sql.append("users.position_id as position_id, ");
			sql.append("positions.name as position_name, ");
			sql.append("users.insert_date as insert_date, ");
			sql.append("users.resurrection as resurrection ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN positions ");
			sql.append("ON users.position_id = positions.id ");
			sql.append("ORDER BY id DESC");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserManagement> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else {
				return userList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}



	private List<UserManagement> toUserList(ResultSet rs) throws SQLException {

		List<UserManagement> ret = new ArrayList<UserManagement>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String userName = rs.getString("user_name");
				int branchId = rs.getInt("branch_id");
				String branchName = rs.getString("branch_name");
				int positionId = rs.getInt("position_id");
				String positionName = rs.getString("position_name");
				Timestamp insertDate = rs.getTimestamp("insert_date");
				int resurrection = rs.getInt("resurrection");

				UserManagement user = new UserManagement();
				user.setId(id);
				user.setUserName(userName);
				user.setBranchId(branchId);
				user.setBranchName(branchName);
				user.setPositionId(positionId);
				user.setPositionName(positionName);
				user.setInsertDate(insertDate);
				user.setResurrection(resurrection);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
