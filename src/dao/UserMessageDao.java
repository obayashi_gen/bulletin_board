package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> getUserMessages(Connection connection, int num, String startDate, String endDate, String category) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("messages.title as title, ");
			sql.append("messages.text as text, ");
			sql.append("messages.category as category, ");
			sql.append("messages.user_id as user_id, ");
			sql.append("users.name as name, ");
			sql.append("messages.insert_date as insert_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			if (category != null && category != "") {
				sql.append("WHERE messages.category LIKE ? ");
			}
			sql.append("AND messages.insert_date BETWEEN ? AND ? ");
			sql.append("ORDER BY insert_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			if (category != null && category != "") {
				ps.setString(1, "%" + category + "%");
				ps.setString(2, startDate);
				ps.setString(3, endDate);
			} else {
				ps.setString(1, startDate);
				ps.setString(2, endDate);
			}

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				String title = rs.getString("title");
				String name = rs.getString("name");
				String category = rs.getString("category");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String text = rs.getString("text");
				Timestamp insertDate = rs.getTimestamp("insert_date");

				UserMessage message = new UserMessage();
				message.setTitle(title);
				message.setName(name);
				message.setCategory(category);
				message.setId(id);
				message.setUserId(userId);
				message.setText(text);
				message.setInsertDate(insertDate);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}