package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class StopUserFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		int noResurrection = 0;

        HttpSession session = req.getSession();
		User loginUser = (User) session.getAttribute("loginUser");

		String servletPath = req.getServletPath();
		String contextPath = req.getContextPath();
		String loginPath = "/login";

        if((servletPath.equals(loginPath) || servletPath.matches("/css/.*")) || (loginUser.getResurrection() == noResurrection)){
            chain.doFilter(req, res);
        } else {
        	List<String> errorMessages = new ArrayList<String>();
        	errorMessages.add("アカウント停止中です");
        	req.getSession().setAttribute("errorMessages", errorMessages);
        	res.sendRedirect(contextPath + loginPath);
        }
    }

	public void init(FilterConfig config) throws ServletException{}
    public void destroy(){}
}
