package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter({"/userManagement", "/settings", "/signup"})
public class UserManagementFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		int HeadOfficeId = 1;
		int GeneralAffairsId = 2;

        HttpSession session = req.getSession();
		User loginUser = (User) session.getAttribute("loginUser");

        if(loginUser.getBranchId() == HeadOfficeId && loginUser.getPositionId() == GeneralAffairsId){
            chain.doFilter(req, res);
        } else {
        	List<String> errorMessages = new ArrayList<String>();
        	errorMessages.add("本社総務部の人しかユーザー管理画面、ユーザー編集画面に遷移できません");
        	req.getSession().setAttribute("errorMessages", errorMessages);

        	res.sendRedirect("./");
        }
    }

	public void init(FilterConfig config) throws ServletException{}
    public void destroy(){}
}
