package filter;

import	java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import	javax.servlet.Filter;
import	javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import	javax.servlet.ServletException;
import	javax.servlet.ServletRequest;
import	javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		String servletPath = req.getServletPath();
		String contextPath = req.getContextPath();
		String loginPath = "/login";
//		String logoutPath = "/logout";

        HttpSession session = req.getSession();
		User loginUser = (User) session.getAttribute("loginUser");

        if(loginUser != null || servletPath.equals(loginPath) || servletPath.equals("/css/style.css")) {
            chain.doFilter(req, res);
        } else {
        	List<String> errorMessages = new ArrayList<String>();
        	errorMessages.add("ログインしてください");
        	req.getSession().setAttribute("errorMessages", errorMessages);

        	res.sendRedirect(contextPath + loginPath);
		}
    }

	public void init(FilterConfig config) throws ServletException{}
    public void destroy(){}
}
