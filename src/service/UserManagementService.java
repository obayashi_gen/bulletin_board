package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.UserManagement;
import dao.UserManagementDao;

public class UserManagementService {

	public List<UserManagement> getUser() {

		Connection connection = null;
		try {
			connection = getConnection();

			UserManagementDao UserManagementDao = new UserManagementDao();
			List<UserManagement> user = UserManagementDao.getAllUser(connection);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}