package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import dao.StopResurrectionDao;

public class StopResurrectionService {

	public void update(int id, int resurrection) {

		Connection connection = null;
		try {
			connection = getConnection();

			StopResurrectionDao stopResurrectionDao = new StopResurrectionDao();
			stopResurrectionDao.update(connection, id, resurrection);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
