package beans;

import java.io.Serializable;
import java.util.Date;

public class UserManagement implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String userName;
	private int branchId;
	private int positionId;
	private String branchName;
	private String positionName;
	private Date insertDate;
	private int resurrection;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getBranchId() {
		return branchId;
	}
	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}
	public int getPositionId() {
		return positionId;
	}
	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getPositionName() {
		return positionName;
	}
	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}
	public Date getInsertDate() {
		return insertDate;
	}
	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}
	public int getResurrection() {
		return resurrection;
	}
	public void setResurrection(int resurrection) {
		this.resurrection = resurrection;
	}

	@Override
	public String toString() {
		return String.format("id=%s, name=%s, branch_id=%s, position_id=%s, branch_name=%s, position_name=%s, insertDate=%s, resurrection=%s",
				id, userName, branchId, positionId, branchName, positionName, insertDate, resurrection );
	}
}