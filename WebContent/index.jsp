<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="./js/ajax.js"></script>
</head>
<body>


	<section class='message-area'>
		<section class='view-area'>
			<p>メッセージ1</p>
		</section>
		<section class='comment-area'>
			<section class='view-area'>
				<p>コメント1-1</p>
				<p>コメント1-2</p>
				<p>コメント1-3</p>
			</section>
			<section class='error-area'>
				<ul></ul>
			</section>
			<section class='form-area'>
				<form id='register-form'>
					<input type='hidden' id='message_id' value='10'> <input
						type='hidden' id='user_id' value='3'>

					<div>
						<textarea rows="5" cols="20" id='text' class='comment-text-area'></textarea>
					</div>
					<div>
						<button type='button' id='register'>登録</button>
					</div>
				</form>
			</section>
		</section>
	</section>

	<hr style="margin_buttom: 30px;">

	<section class='message-area'> <section class='view-area'>
	<p>メッセージ2</p>
	</section> <section class='comment-area'> <section class='view-area'>
	<p>コメント2-1</p>
	<p>コメント2-2</p>
	<p>コメント2-3</p>
	</section> <section class='error-area'>
	<ul></ul>
	</section> <section class='form-area'>
	<form id='register-form'>
		<input type='hidden' id='message_id' value='14'> <input
			type='hidden' id='user_id' value='3'>

		<div>
			<textarea rows="5" cols="20" id='text' class='comment-text-area'></textarea>
		</div>
		<div>
			<button type='button' id='register'>登録</button>
		</div>
	</form>
	</section> </section> </section>

	<hr style="margin_buttom: 30px;">

	<section class='message-area'> <section class='view-area'>
	<p>メッセージ3</p>
	</section> <section class='comment-area'> <section class='view-area'>
	<p>コメント3-1</p>
	<p>コメント3-2</p>
	<p>コメント3-3</p>
	</section> <section class='error-area'>
	<ul></ul>
	</section> <section class='form-area'>
	<form id='register-form'>
		<input type='hidden' id='message_id' value='22'> <input
			type='hidden' id='user_id' value='3'>

		<div>
			<textarea rows="5" cols="20" id='text' class='comment-text-area'></textarea>
		</div>
		<div>
			<button type='button' id='register'>登録</button>
		</div>
	</form>
	</section> </section> </section>
</body>
</html>
