<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>トップ</title>
<link rel="stylesheet"
	href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="./js/script.js"></script>
<script type="text/javascript" src="./footerFixed.js"></script>
</head>

<body>
	<div class="header-nav">
		<ul class="header-nav_list">
			<li class="left-menu"><a href="./">ホーム</a></li>
			<li class="left-menu"><a href="newMessage">新規投稿</a></li>
			<li class="left-menu"><c:if
					test="${loginUser.getBranchId() == 1 && loginUser.getPositionId() == 2}">
					<a href="userManagement">ユーザ管理</a>
				</c:if></li>
			<li class="right-menu"><a href="logout">ログアウト</a></li>
		</ul>
	</div>

	<div class="contents">
		<div class="left-content">
			<div class="left-top-content">
				<div class="profile">
					<div class="user-name">
						<c:out value="${loginUser.name}" />
					</div>
					<div class="branch-name">
						支店：
						<c:out value="${branch.name}" />
					</div>
					<div class="position-name">
						部署・役職：
						<c:out value="${position.name}" />
					</div>
				</div>
			</div>

			<div class="left-bottom-content">
				<div>
					<div class="narrowing-title">&lt;絞込み検索&gt;</div>
					<br>
					<form action="./" method="get">
						<label>カテゴリー<br /> <input type="text" name="category"
							class="category-input" value="${category}">
						</label><br />
						<br> <label>投稿日<br /> <input type="date"
							name="startDate" value="${startDate}"> <br /> ～ <br />
							<input type="date" name="endDate" value="${endDate}"> <br />
						</label><br />
						<br> <input type="submit" value="検索"> <a href="./">リセット</a>

					</form>
				</div>
			</div>
		</div>

		<div class="right-content">
			<c:if test="${not empty errorMessages}">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li class="error"><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<c:forEach items="${messages}" var="message">
				<div class="messages">
					<div class="message">
						<table id="message_tbl" border="1">
							<tr>
								<th class="message_tbl-menu"><em>投稿者</em><br></th>
								<td class="message_tbl-content">
									<div class="name">
										<c:out value="${message.name}" />
									</div>
								</td>
							</tr>
							<tr>
								<th class="message_tbl-menu"><em>件名</em><br></th>
								<td class="message_tbl-content">
									<div class="title">
										<c:out value="${message.title}" />
									</div>
								</td>
							</tr>
							<tr>
								<th class="message_tbl-menu"><em>カテゴリー</em><br></th>
								<td class="message_tbl-content">
									<div class="category">
										<c:out value="${message.category}" />
									</div>
								</td>
							</tr>
							<tr>
								<th class="message_tbl-menu"><em>本文</em><br></th>
								<td class="message_tbl-content">
									<div class="text">
										<c:out value="${message.text}" />
									</div>
								</td>
							</tr>
							<tr>
								<th class="message_tbl-menu"><em>投稿日時</em><br></th>
								<td class="message_tbl-content">
									<div class="date">
										<fmt:formatDate value="${message.insertDate}"
											pattern="yyyy/MM/dd HH:mm:ss" />
									</div>
								</td>
							</tr>
						</table>

						<c:if test="${loginUser.id == message.userId}">
							<form action="deleteMessage" method="post" class="form-delete-message">
								<input name="message-id" type="hidden" id="message-id" class="message-id" value="<c:out value="${message.id}" />" />
								<button type="button" class="delete-message-btn">削除</button>
							</form>
						</c:if>
					</div><br>

					<!-- コメント投稿エリア -->
					<div class="right-content-bottom">
		 				<div class="top-form-area">
							<section class='comment-area2'>
								<section class='error-area'>

								</section>
								<section class='form-area'>
									<form id='register-form'>
										<input type= "hidden" class= "message-id" id= "message_id" value="${message.id}" />
										<input type= "hidden" class= "comment-user-box" id= "user_id" value="${loginUser.id}" />
										<div class="comment">コメント本文（500文字以下）</div>
										<textarea placeholder= "コメントを入力してください" cols="20" rows="2" id="comment_text" class="comment_text"><c:if test= "${message.id == comment.messageId}">${comment.text}</c:if></textarea>
										<button type='button' id='register'>コメント投稿</button>
									</form>
								</section>
							</section>
						</div>

						<div class="comments">
							<em class="line">--------------------------コメント一覧--------------------------</em>
							<section class='message-area'>
								<section class='comment-area'>
									<section class='view-area'>
										<div>
											<section class="new-comment"></section>
										</div>
										<c:forEach items="${comments}" var="comment">
											<c:if test="${message.id == comment.messageId}">
												<div class="comment">
													<div class="name">
														コメント投稿者：
														<c:out value="${comment.name}" />
													</div>
													<div class="text">
														<c:forEach
															items="${fn:split(comment.text, '
																	')}"
															var="text">
															<c:out value="${text}" />
															<br />
														</c:forEach>
													</div>
													<div class="date">
														投稿日時：
														<fmt:formatDate value="${comment.insertDate}"
															pattern="yyyy/MM/dd HH:mm:ss" />
													</div>

													<c:if test="${loginUser.id == comment.userId}">
														<form action="deleteComment" method="post" class="form-delete-comment">
															<input name="comment-id" type="hidden" id="comment-id" class="comment-id" value="<c:out value="${comment.id}" />" />
															<button type="button" class="delete-comment-btn">削除</button>
														</form>
													</c:if>
												</div>
												<br />
											</c:if>
										</c:forEach>
									</section>
								</section>
							</section>


							<!--
							<c:forEach items="${comments}" var="comment">
								<c:if test= "${message.id == comment.messageId}">
									<div class="comment">
										<div class="name">コメント投稿者：<c:out value="${comment.name}" /></div>
										<div class="text">
											<c:forEach items="${fn:split(comment.text, '
											')}" var="text">
											    <c:out value="${text}" />
											    <br />
											</c:forEach>
										</div>
										<div class="date">投稿日時：<fmt:formatDate value="${comment.insertDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>

										<c:if test="${loginUser.id == comment.userId}">
											<form action="deleteComment" method="post" class="form-delete-comment" >
												<input name= "comment-id" type= "hidden" id= "comment-id" class= "comment-id" value="<c:out value="${comment.id}" />"/>
												<button type="button" class="delete-comment-btn">削除</button>
											</form>
										</c:if>
									</div><br />
								</c:if>
							</c:forEach>
-->
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
	<div id="footer">
		<div class="copyright">Copyright(c)Gen Obayashi</div>
	</div>
	<div class="delete-message" style="display: none;"></div>
	<div class="delete-comment" style="display: none;"></div>
</body>
</html>
