$(function() {
	$('button#register').on('click', function() {
		// 送信するデータを用意
		var form = $(this).parents("form#register-form");
		var messageId = form.find('#message_id').val();
		var userId = form.find('#user_id').val();
		var text = form.find('#text').val();
		var comment = { 'message_id': messageId, 'user_id': userId, 'text': text };

		// 全てのerror-areaのメッセージを削除する
		$(".error-area").each(function(k, v){ $(v).find("ul").empty(); })

		// Ajax通信処理
		$.ajax({
			dataType: 'json',
			type: "POST",
			url: 'comment/register',
			data: { comment: JSON.stringify(comment) }
		}).done(function(data, textStatus, jqXHR) {
			// 成功時の処理
			if(data.is_success == 'true') {
				/* バリデーション通過 */
				// コメントデータを追加
				form.parents("section.comment-area").find("section.view-area").append("<p>" + text + "</p>");
				// 全てのtextarea要素の中身を削除
				$(".comment-text-area").each(function(k, v) { $(this).val(""); })
			} else {
				/* バリデーションエラー */
				var errorArea = form.parents("section.comment-area").find("section.error-area");
				// 入力フォームの上にエラーメッセージを表示させる
				data.errors.forEach(function(v, k) {
					// エラーメッセージの要素の数だけ表示させる
					errorArea.find("ul").append("<li>" + v + "</li>");
				});
			}
		}).fail(function(data, textStatus, jqXHR) {
			// 通信失敗時の処理
			console.log(data);
			console.log('error!!');
		});
	});
});
