$(function() {
	$(".user-stop-btn").click(function() {
		var hoge = $(this);
		$(".user-stop").dialog({
			modal:true, //モーダル表示
			title:"ユーザを停止しますか？", //タイトル
			buttons: { //ボタン
			"停止する": function() {
				hoge.parent().submit();
				$(this).dialog("close");
				},
			"キャンセル": function() {
				$(this).dialog("close");
				}
			}
		});
	});

	$(".user-resurrection-btn").click(function() {
		var hoge = $(this);
		$(".user-resurrection").dialog({
			modal:true, //モーダル表示
			title:"ユーザを復活しますか？", //タイトル
			buttons: { //ボタン
			"復活する": function() {
				hoge.parent().submit();
				$(this).dialog("close");
				},
			"キャンセル": function() {
				$(this).dialog("close");
				}
			}
		});
	});

	$(".delete-message-btn").click(function() {
		var hoge = $(this);
		$(".delete-message").dialog({
			modal:true, //モーダル表示
			title:"メッセージを削除しますか？", //タイトル
			buttons: { //ボタン
			"削除する": function() {
				hoge.parent().submit();
				$(this).dialog("close");
				},
			"キャンセル": function() {
				$(this).dialog("close");
				}
			}
		});
	});

	$(".delete-comment-btn").click(function() {
		var hoge = $(this);
		$(".delete-comment").dialog({
			modal:true, //モーダル表示
			title:"コメントを削除しますか？", //タイトル
			buttons: { //ボタン
			"削除する": function() {
				hoge.parent().submit();
				$(this).dialog("close");
				},
			"キャンセル": function() {
				$(this).dialog("close");
				}
			}
		});
	});

//	var boxT   = $(".header-nav");
//    var boxTop = boxT.offset().top;
//    $(window).scroll(function () {
//        if($(window).scrollTop() >= boxTop) {
//        	boxT.addClass("fixed");
//            $("body").css("margin-top","35px");
//        } else {
//        	boxT.removeClass("fixed");
//            $("body").css("margin-top","0px");
//        }
//    });

    $('button#register').on('click', function() {
		// 送信するデータを用意
		var form = $(this).parents("form#register-form");
		var messageId = form.find('#message_id').val();
		var userId = form.find('#user_id').val();
		var text = form.find('#comment_text').val();
		var comment = { 'messageId': messageId, 'userId': userId, 'text': text };

		// 全てのerror-areaのメッセージを削除する
		$(".error-area").each(function(k, v){ $(v).empty(); })

		// Ajax通信処理
		$.ajax({
			dataType: 'json',
			type: "POST",
			url: 'comment',
			data: { comment: JSON.stringify(comment) }
		}).done(function(data, textStatus, jqXHR) {
			// 成功時の処理
			if(data.is_success == 'true') {
				/* バリデーション通過 */
				// コメントデータを追加

				form.parents(".right-content-bottom").find("section.new-comment")
					.append("<div>コメント投稿者：" + data.loginUserName + "</div>" +
							"<div>" + text + "</div>" +
							"<div>投稿日時：" + data.dateNow + "</div>" +
							"<form action=" + "deleteComment" + " method=" + "post" + " class=" + "form-delete-comment>" +
							"<input name=" + "comment-id" + " type=" + "hidden" + " id=" + "comment-id" + " class=" + "comment-id" + " value=" + data.commentId + " />" +
							"<button type=" + "button" + " class=" + "delete-comment-btn2" + ">削除</button>" +
							"</form><br>"
							);
				// 全てのtextarea要素の中身を削除
				$(".comment_text").each(function(k, v) { $(this).val(""); })
			} else {
				/* バリデーションエラー */
				var errorArea = form.parents("section.comment-area2").find("section.error-area");
				// 入力フォームの上にエラーメッセージを表示させる
				data.errors.forEach(function(v, k) {
					// エラーメッセージの要素の数だけ表示させる
					errorArea.append("<div class=" + "comment-error" + ">" + v + "</div>");
				});
			}
		}).fail(function(data, textStatus, jqXHR) {
			// 通信失敗時の処理
			console.log(data);
			console.log('error!!');
		});
	});

    $(document).on('click', '.delete-comment-btn2', function() {
    	var hoge = $(this);
		$(".delete-comment").dialog({
			modal:true, //モーダル表示
			title:"コメントを削除しますか？", //タイトル
			buttons: { //ボタン
			"削除する": function() {
				hoge.parent().submit();
				$(this).parents('section.new-comment').remove();
				$(this).dialog("close");
				},
			"キャンセル": function() {
				$(this).dialog("close");
				}
			}
		});

	});
});
