<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ログイン</title>
	<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
		<c:if test="${not empty errorMessages}">
			<div class= "errorMessages">
				<ul>
					<c:forEach items= "${errorMessages}" var= "message">
						<li class="error"><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope= "session"/>
		</c:if>

		<h1 class= "loginTitle">ログイン画面</h1>

		<div class= "login-outer">
			<form action="login" class="login-form" method="post"><br />
				<label for="loginId" id="login-id-label">ログインID</label><br>
				<input name="loginId" id="login-id" value="${loginID}"/> <br /><br/>

				<label for="password" id="login-password-label">パスワード</label><br>
				<input name="password" type="password" id="login-password" value="${password}"/> <br /><br/>

				<input type="submit"id="login-submit" value="ログイン" /> <br />
			</form><br/>
		</div>

	<div id="footer">
		<div class="copyright">Copyright(c)Gen Obayashi</div>
	</div>
</body>
</html>
