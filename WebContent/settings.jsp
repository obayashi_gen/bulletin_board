<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー編集</title>
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
	<script src="./js/script.js"></script>
</head>
<body>

	<div class="header-nav">
		<ul class= "header-nav_list">
			<li class="right-menu"><a href="logout">ログアウト</a></li>
			<li class="left-menu"><a href="./">ホーム</a></li>
			<li class="left-menu"><a href="userManagement">ユーザ管理</a></li>
		</ul>
	</div>

	<div class="contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li class="error"><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>


		<form action="settings" method="post"><br />
			<input type="hidden" name="id" value="${editUser.id}">
			<fmt:formatDate value="${editUser.updateDate}" pattern="yyyy-MM-dd HH:mm:ss" var="update_date"/>
			<input type="hidden" name="update_date" value="${update_date}">

			<table id="user_tbl" border="1">
				<tr>
					<th class="user_tbl-menu">
						<em>ログインID</em><br>
						<em>&lt;必須&gt;</em><br>
					</th>
					<td class="user_tbl-content">
						<em>&lt;半角英数字で6文字以上20文字以下&gt;</em><br>
						<input name="loginId" value="${editUser.loginId}" />
					</td>
				</tr>
				<tr>
					<th class="user_tbl-menu">
						<em>パスワード</em><br>
						<em>&lt;空欄の場合は変更されません&gt;</em><br>
					</th>
					<td class="user_tbl-content">
						<em>&lt;記号を含む全ての半角文字で6文字以上20文字以下&gt;</em><br>
						<input name="password" type="password" id="password" /><br>

						<em>&lt;確認用&gt;</em><br>
						<input name="checkPassword" type="password" id="password" />
					</td>
				</tr>
				<tr>
					<th class="user_tbl-menu">
						<em>名称</em><br>
						<em>&lt;必須&gt;</em><br>
					</th>
					<td class="user_tbl-content">
						<em>&lt;10文字以下&gt;</em><br>
						<input name="name" value="${editUser.name}" id="name"/>
					</td>
				</tr>
				<tr>
					<th class="user_tbl-menu">
						<em>支店</em><br>
						<em>&lt;必須&gt;</em><br>
					</th>
					<td class="user_tbl-content">
						<c:choose>
							<c:when test="${editUser.id == loginUser.id}">
								<input type="hidden" name="branch" value="${editUser.branchId}">
								<c:forEach items="${branches}" var="branch">
									<c:if test="${branch.id == editUser.branchId}">
										<option class= "branch" value= "${branch.id}" selected>${branch.name}</option>
									</c:if>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<select name="branch">
									<c:forEach items="${branches}" var="branch">
										<c:choose>
											<c:when test="${branch.id == editUser.branchId}">
												<option class= "branch" value= "${branch.id}" selected><c:out value="${branch.name}" /></option>
											</c:when>
											<c:otherwise>
												<option class= "branch" value= "${branch.id}"><c:out value="${branch.name}" /></option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</select>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr>
					<th class="user_tbl-menu">
						<em>部署・役職</em><br>
						<em>&lt;必須&gt;</em><br>
					</th>
					<td class="user_tbl-content">
						<c:choose>
							<c:when test="${editUser.id == loginUser.id}">
								<input type="hidden" name="position" value="${editUser.positionId}">
								<c:forEach items="${positions}" var="position">
									<c:if test="${position.id == editUser.positionId}">
										<option class= "position" value= "${position.id}" selected>${position.name}</option>
									</c:if>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<select name="position">
									<c:forEach items="${positions}" var="position">
										<c:choose>
											<c:when test="${position.id == editUser.positionId}">
												<option class= "position" value= "${position.id}" selected><c:out value="${position.name}" /></option>
											</c:when>
											<c:otherwise>
												<option class= "position" value= "${position.id}"><c:out value="${position.name}" /></option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</select>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</table>
			<input type="submit" id="edit-submit" value="登録" />
		</form>
	</div>

	<div id="footer">
		<div class="copyright">Copyright(c)Gen Obayashi</div>
	</div>
</body>
</html>
