<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>新規投稿</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="header-nav">
		<ul class= "header-nav_list">
			<li class="right-menu"><a href="logout">ログアウト</a></li>
			<li class="left-menu"><a href="./">ホーム</a></li>
		</ul>
	</div>

	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li class="error"><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session"/>
	</c:if>

	<div class="contents">
		<form action="newMessage" method="post" class="new_message-form-area">
			<label for= "title">
				件名（30文字以下）
			</label><br />
			<input name= "title" id= "title" class= "new_message-title-box" value="${message.title}" /> <br /><br />


			<label for= "message">
				本文（1000文字以下）<br />
			</label>
			<textarea name="text" cols="10" rows="25"  class="new_messaget-box" >${message.text}</textarea><br /><br />


			<label for= "category">
				カテゴリー（10文字以下）
			</label><br />
			<input name= "category" id= "category" class="new_message-category-box" value="${message.category}" /><br /><br />

			<input type="submit" value="投稿">
		</form>
	</div>
	<div id="footer">
		<div class="copyright">Copyright(c)Gen Obayashi</div>
	</div>
</body>
</html>