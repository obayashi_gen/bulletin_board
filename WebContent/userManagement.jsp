<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー管理</title>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script src="./js/script.js"></script>
</head>
<body>
	<div class="header-nav">
		<ul class= "header-nav_list">
			<li class="left-menu"><a href="./">ホーム</a></li>
			<li class="left-menu"><a href="signup">ユーザー登録</a></li>
			<li class="right-menu"><a href="logout">ログアウト</a></li>
		</ul>
	</div>

	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li class="error"><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session"/>
	</c:if>

	<div class="contents">
		<table id="user_tbl" border="1">
			<tr>
				<td class="user_tbl-Row">名称</td>
				<td class="user_tbl-Row">支店</td>
				<td class="user_tbl-Row">部署・役職</td>
				<td class="user_tbl-Row">ユーザー編集</td>
				<td class="user_tbl-Row">停止/復活</td>
			</tr>
			<c:forEach items="${users}" var="user">
				<tr class="user">
					<td class="name"><c:out value="${user.userName}" /></td>
					<td class="branch"><c:out value="${user.branchName}" /></td>
					<td class="position"><c:out value="${user.positionName}" /></td>
					<td class="edit-form">
						<form action="settings" method="get">
							<input name="userId" type="hidden" value="<c:out value="${user.id}" />"/>
							<input type="submit" value="ユーザ編集"/>
						</form>
					</td>
					<td>
						<form action="stopResurrection" method="post" class="form-stopResurrection" >
							<c:choose>
								<c:when test="${user.resurrection == 0}">
									<input name="userId" type="hidden" value="<c:out value="${user.id}" />"/>
									<input name="resurrection" type="hidden" value="<c:out value="${1}" />"/>
									<input type="button" class="user-stop-btn" value="停止" />
								</c:when>
								<c:otherwise>
									<input name="userId" type="hidden" value="<c:out value="${user.id}" />"/>
									<input name="resurrection" type="hidden" value="<c:out value="${0}" />"/>
									<input type="button" class="user-resurrection-btn" value="復活" />
								</c:otherwise>
							</c:choose>
						</form>
					</td>
				<tr>
			</c:forEach>
		</table>
	</div>
	<div id="footer">
		<div class="copyright">Copyright(c)Gen Obayashi</div>
	</div>
</body>
<div class="user-stop" style="display:none;"></div>
<div class="user-resurrection" style="display:none;"></div>
</html>