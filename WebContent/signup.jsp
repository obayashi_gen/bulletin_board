<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@page isELIgnored="false" %>
 <%@taglib prefix= "c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー登録</title>
	<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="header-nav">
		<ul class= "header-nav_list">
			<li class="right-menu"><a href="logout">ログアウト</a></li>
			<li class="left-menu"><a href="./">ホーム</a></li>
			<li class="left-menu"><a href="userManagement">ユーザ管理</a></li>
		</ul>
	</div>

	<div class= "contents">
		<c:if test="${not empty errorMessages}">
			<div class= "errorMessages">
				<ul>
					<c:forEach items= "${errorMessages}" var= "message">
						<li class="error"><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope= "session"/>
		</c:if>



		<form action="signup" method= "post" class="signup-form">
			<table id="user_tbl" border="1">
				<tr>
					<th class="user_tbl-menu">
						<em>ログインID</em><br>
						<em>&lt;必須&gt;</em><br>
					</th>
					<td class="user_tbl-content">
						<em>&lt;半角英数字で6文字以上20文字以下&gt;</em><br>
						<input name= "loginId" id= "loginId" value="${signUpUser.loginId}" />
					</td>
				</tr>
				<tr>
					<th class="user_tbl-menu">
						<em>パスワード</em><br>
						<em>&lt;必須&gt;</em><br>
					</th>
					<td class="user_tbl-content">
						<em>&lt;記号を含む全ての半角文字で6文字以上20文字以下&gt;</em><br>
						<input name= "password" type= "password" id= "password" /><br>

						<em>&lt;確認用&gt;</em><br>
						<input name="checkPassword" type="password" id="password" />
					</td>
				</tr>
				<tr>
					<th class="user_tbl-menu">
						<em>名称</em><br>
						<em>&lt;必須&gt;</em><br>
					</th>
					<td class="user_tbl-content">
						<em>&lt;10文字以下&gt;</em><br>
						<input name= "name" id= "name" value="${signUpUser.name}" />
					</td>
				</tr>
				<tr>
					<th class="user_tbl-menu">
						<em>支店</em><br>
						<em>&lt;必須&gt;</em><br>
					</th>
					<td class="user_tbl-content">
						<select name="branch">
							<c:forEach items="${branches}" var="branch">
								<c:choose>
									<c:when test="${branch.id == signUpUser.branchId}">
										<option class= "branch" value= "${branch.id}" selected><c:out value="${branch.name}" /></option>
									</c:when>
									<c:otherwise>
										<option class= "branch" value= "${branch.id}"><c:out value="${branch.name}" /></option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						 </select>
					</td>
				</tr>
				<tr>
					<th class="user_tbl-menu">
						<em>部署・役職</em><br>
						<em>&lt;必須&gt;</em><br>
					</th>
					<td class="user_tbl-content">
						<select name="position">
							<c:forEach items="${positions}" var="position">
								<c:choose>
									<c:when test="${position.id == signUpUser.positionId}">
										<option class= "position" value= "${position.id}" selected><c:out value="${position.name}" /></option>
									</c:when>
									<c:otherwise>
										<option class= "position" value= "${position.id}"><c:out value="${position.name}" /></option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						 </select>
					</td>
				</tr>
			</table>
			<input type="submit" id="signup-submit" value="登録" />
		</form>
	</div>

	<div id="footer">
		<div class="copyright">Copyright(c)Gen Obayashi</div>
	</div>
</body>
</html>





